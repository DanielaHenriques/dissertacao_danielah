README.txt

Os scripts dos modelos implementados estão presentes na pasta 'Scripts' e nas pastas dos satélites à qual foram testados.

Como muitos dados foram injetados no Google Earth Engine, nomeadamente, imagens Sentinel 2 e shapefiles, 
para testar tudo seria necessário partilhar por email a coleção de scrips no próprio GEE para ser possível ter acesso
a todos os dados e testar.

Se for necessário, é só contactar (ds.henriques@campus.fct.unl.pt).

Para rápido acesso, basta aceder aos respectivos url da pasta 'URLS' para testar alguns modelos.
Dados como métricas e exportar imagens, só é possível com acesso à consola, portanto no próprio Google Earth Engine com acesso a todos os dados.

